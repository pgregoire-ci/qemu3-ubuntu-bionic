#!/bin/sh
##
set -eu

sed -i -E -e 's|.*(deb-src.*)|\1|' /etc/apt/sources.list
apt-get update

apt-get -y install \
	checkinstall \
	git \
	libgtk-3-dev \
	libvte-2.91-dev

apt-get -y build-dep qemu
apt-get clean
rm -rf /var/lib/apt/lists/*

git clone https://github.com/qemu/qemu /usr/src/qemu
cd /usr/src/qemu
git checkout v3.0.0
git submodule init
git submodule update --recursive

./configure \
	--enable-gtk \
	--with-gtkabi=3.0 \
	--enable-vte \
	--disable-sdl \

make
checkinstall -y --pkgversion=3.0.0 make install
